![alt text](https://i.imgur.com/KDfmnsU.png "Covid19Dashboard")

# Backup-CLI

**Backup-CLI is a simple backup and archiving tool.**
You can archive folders or files locally and on remote machines or build groups from different paths and then bulk-archive them.

The concept is a simple and easy to adapt tool that can be executed via cron job or in automations. It is built in Node.js and Vorpal.

## Create Simple Backup

To create a backup, you can simply start the script with the following command. 

``` cli
backupcli: backup file
```

or 

``` cli
backupcli: backup folder
```

[Gif Placeholder]

## Create Group

The purpose of group is to **unite several paths and archive them together.** 
Example: Create an archive of web-root folders from multiple web servers.

First you have to create a group:

``` cli
backupcli: group create
```

[Gif Placeholder]

## Add Path to Group

Once a group has been created, you can then start adding paths. **All paths in a group will then be used for a backup.**

``` cli
backupcli: group add
```
[Gif Placeholder]

## Create a Group Backup

If all paths in a group are added, a backup can be created. This will be stored at the location defined above. 

``` cli
backupcli: backup group 
```
[Gif Placeholder]

## Dependencies

``` json
 "dependencies": {
    "archiver": "^3.0.0",
    "chalk": "^2.4.2",
    "cli-progress": "^2.1.1",
    "cli-spinner": "^0.2.10",
    "colors": "^1.3.3",
    "commander": "^2.20.0",
    "figures": "^3.0.0",
    "inquirer": "^6.4.1",
    "lodash": "^4.17.15",
    "moment": "^2.24.0",
    "vorpal": "^1.12.0",
    "zip-folder": "^1.0.0"
 }
```

## License

[ISC License ](https://choosealicense.com/licenses/isc/)

**Copyright (c) 2019, Julian Haugeneder** 

Permission to use, copy, modify, and/or distribute this software for any  
purpose with or without fee is hereby granted, provided that the above  
copyright notice and this permission notice appear in all copies.  

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES  
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF  
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR  
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES  
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN  
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF  
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

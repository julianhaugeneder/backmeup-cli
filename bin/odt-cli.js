#!/usr/bin/env node

const fs = require("fs");
const db = JSON.parse(fs.readFileSync("bin/db.json"));
const chalk = require('chalk');
const lodash_ = require('lodash');
const moment = require('moment');
const {description, version} = require('../package.json')
const vorpal = require('vorpal')();
const archiver = require('archiver');
const figures = require('figures');
const _cliProgress = require('cli-progress');
const path = require("path");
var fsUtils = require("nodejs-fs-utils");

let output;

let banner = "  ___          _                  ___ _    ___ \n" +
    " | _ ) __ _ __| |___  _ _ __ ___ / __| |  |_ _|\n" +
    " | _ \\/ _` / _| / / || | '_ \\___| (__| |__ | | \n" +
    " |___/\\__,_\\__|_\\_\\\\_,_| .__/    \\___|____|___|\n" +
    "                       |_|                     "

function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}


vorpal
    .delimiter(chalk.yellowBright.bold('Backup-CLI:'))
    .log(chalk.yellowBright.bold(banner))
    .log(chalk.gray.bold("Version: " + version))
    .log(chalk.gray.bold(description + "\nYou can archive folders or files locally and remote machines or build groups from different paths and then bulk-archive them."))
    .log(chalk.gray.bold("By Julian Haugeneder @2020"))
    .log()
    .show();


vorpal
    .command('group create', 'Create a new group')
    .action(function (args, cb) {

        let questions = [
            {
                type: 'input',
                name: 'group',
                message: 'Enter the new group name: '
            },
            {
                type: 'input',
                name: 'savePath',
                message: 'Where should the group be archived? '
            }
        ];


        let self = this;
        this.prompt(questions, function (answers) {

            let group = answers.group;
            let savePath = answers.savePath;

            group = group.toLowerCase()

            if (typeof db.locations.groups[group] == "object") {
                self.log(chalk.bold.redBright(""));
                self.log(chalk.bold.redBright("────────────────────────────────────────"));
                self.log(chalk.bold.redBright("[LOG]: Error! " + figures.cross + " Group already exists"));
                self.log(chalk.bold.redBright("────────────────────────────────────────"));
            } else {
                db.locations.groups[group] = new Object();
                db.locations.groups[group].dirs = new Array()
                db.locations.groups[group].savePath = savePath.toString()
                fs.writeFileSync("bin/db.json", JSON.stringify(db));
                self.log(chalk.bold.greenBright(""));
                self.log(chalk.bold.greenBright("────────────────────────────────────────"));
                self.log(chalk.greenBright.bold("[LOG]: Success! " + figures.tick + " New Group '" + group + "' Added"));
                self.log(chalk.bold.greenBright("────────────────────────────────────────"));
            }
            cb()
        })
    });

vorpal
    .command('group add', 'Add a path to a group')
    .action(function (args, cb) {
        let self = this;

        let questions = [
            {
                type: 'input',
                name: 'group',
                message: 'To which group should the entry be added? '
            },
            {
                type: 'input',
                name: 'path',
                message: 'Which path should be added? '
            }
        ];

        this.prompt(questions, function (answers) {

            let group = answers.group
            group = group.toLowerCase()

            if (typeof db.locations.groups[group] == "object") {
                db.locations.groups[group].dirs.push(answers.path)
                fs.writeFileSync("bin/db.json", JSON.stringify(db));
                self.log(chalk.bold.greenBright(""));
                self.log(chalk.bold.greenBright("────────────────────────────────────────"));
                self.log(chalk.greenBright.bold("[LOG]: Success! " + figures.tick + " Location '" + answers.path + "' added to Group '" + group + "'"));
                self.log(chalk.bold.greenBright("────────────────────────────────────────"));
            } else {
                self.log(chalk.bold.redBright(""));
                self.log(chalk.bold.greenBright("────────────────────────────────────────"));
                self.log(chalk.bold.redBright("[LOG]: Error! " + figures.cross + " Group not found!"));
                self.log(chalk.bold.redBright("────────────────────────────────────────"));
            }
            cb()
        })
    });

vorpal
    .command('group show', 'Display all groups')
    .action(function (args, cb) {

        let db = fs.readFileSync("bin/db.json")
        let output = JSON.stringify(JSON.parse(db).locations.groups, null, 2)
        output = output.replace(/[{}]/g, '');
        output = output.replace(/[""]/g, '');
        output = output.replace(/[\[\]']+/g, '');



        this.log(output)


    });


vorpal
    .command('backup file', 'Backup a single file')
    .action(function (args, cb) {
        let self = this;
        let questions = [
            {
                type: 'input',
                name: 'file',
                message: 'Which file should be saved?'
            },
            {
                type: 'input',
                name: 'save',
                message: 'Where should the file be archived? '
            }
        ];


        this.prompt(questions, function (answers) {

            let archive = archiver('zip', {
                zlib: {level: 9}
            });

            output = fs.createWriteStream(answers.save + '/' + moment().format('DDMMYYYY-HHmmss').toString() + '_Backup.zip');
            self.archive = archiver('zip', {
                zlib: {level: 9}
            });
            const bar1 = new _cliProgress.Bar({
                format: 'Backup Progress |' + '{bar}' + '| {percentage}% || {value}/{total} Chunks || ETA {eta_formatted} || Running {duration_formatted}',
                barCompleteChar: '\u2588',
                barIncompleteChar: '\u2591',
                stream: process.stdout,
                barsize: 32,
            }, _cliProgress.Presets.shades_classic);

            self.log('');
            self.log(chalk.bold.greenBright("────────────────────────────────────────"));
            self.log(chalk.greenBright.bold("Collecting Files..."))

            let file = fs.createReadStream(answers.file);
            let totalFileSize = fs.statSync(answers.file).size
            let filecount=1;

            self.log(chalk.greenBright.bold("Size: "+formatBytes(totalFileSize)))
            self.log(chalk.greenBright.bold("Files: "+ filecount))
            self.log(chalk.bold.greenBright("────────────────────────────────────────"));
            self.log('');

            chalk.greenBright.bold(bar1.start(100, 0));
            archive.append(file, {name: path.basename(answers.file)})
            archive.finalize()
            archive.pipe(output);

            let myVar = setInterval(function(){ timer() }, 3000);
                        function timer() {
                var percent = Math.floor(100-((totalFileSize - archive.pointer()) / totalFileSize) * 100);
                bar1.update(percent)

            }




            output.on('close', function () {
                bar1.stop()
                clearInterval(myVar);
                self.log('');
                self.log(chalk.bold.greenBright("────────────────────────────────────────"));
                self.log(chalk.greenBright.bold("[LOG]: Success! " + figures.tick));
                self.log(chalk.greenBright.bold("Size: " + Math.floor(archive.pointer() / 1000000) + " MB"));
                self.log(chalk.bold.greenBright("────────────────────────────────────────"));
                self.log('');
                cb()
            });

            archive.on('warning', function(err) {
                bar1.stop()
                clearInterval(myVar);
                self.log(chalk.bold.yellowBright(""));
                self.log(chalk.bold.yellowBright("────────────────────────────────────────"));
                self.log(chalk.bold.yellowBright("[LOG]: Warning! " + figures.cross + err));
                self.log(chalk.bold.yellowBright("────────────────────────────────────────"));
                cb()
            });

            archive.on('error', function(err) {
                bar1.stop()
                clearInterval(myVar);
                self.log(chalk.bold.redBright(""));
                self.log(chalk.bold.redBright("────────────────────────────────────────"));
                self.log(chalk.bold.redBright("[LOG]: Error! " + figures.cross + err));
                self.log(chalk.bold.redBright("────────────────────────────────────────"));
                cb()
            });
        });
    })  .cancel(function () {
    output.close()
});

vorpal
    .command('backup folder', 'Backup a single folder')
    .action(function (args, cb) {
        let self = this;
        let questions = [
            {
                type: 'input',
                name: 'file',
                message: 'Which folder should be saved?'
            },
            {
                type: 'input',
                name: 'save',
                message: 'Where should the folder be archived? '
            }
        ];


        this.prompt(questions, function (answers) {
            output = fs.createWriteStream(answers.save + '/' + moment().format('DDMMYYYY-HHmmss').toString() + '_Backup.zip');
            let archive = archiver('zip', {
                zlib: {level: 9}
            });

            const bar1 = new _cliProgress.Bar({
                format: 'Backup Progress |' + '{bar}' + '| {percentage}% || {value}/{total} Chunks || ETA {eta_formatted} || Running {duration_formatted}',
                barCompleteChar: '\u2588',
                barIncompleteChar: '\u2591',
                stream: process.stdout,
                barsize: 32,
            }, _cliProgress.Presets.shades_classic);

            self.log('');
            self.log(chalk.bold.greenBright("────────────────────────────────────────"));
            self.log(chalk.greenBright.bold("Collecting Files..."))

            let totalFileSize = fsUtils.fsizeSync(answers.file);
            let filecount=0;
            fsUtils.walkSync(answers.file, {
            }, function (err, path, stats, next, cache) {
                if (!err && stats.isFile()) {
                    filecount++;
                }
                next();
            });

            self.log(chalk.greenBright.bold("Size: "+formatBytes(totalFileSize)))
            self.log(chalk.greenBright.bold("Files: "+ filecount))
            self.log(chalk.bold.greenBright("────────────────────────────────────────"));
            self.log('');
            chalk.greenBright.bold(bar1.start(100, 0));
            archive.directory(answers.file, path.basename(answers.file))


            archive.finalize()
            archive.pipe(output);

            let myVar = setInterval(function(){ timer() }, 2000);
            function timer() {
                var percent = Math.floor(100-((totalFileSize - archive.pointer()) / totalFileSize) * 100);
                bar1.update(percent)
            }



            output.on('close', function () {
                bar1.stop()
                clearInterval(myVar);
                self.log('');
                self.log(chalk.bold.greenBright("────────────────────────────────────────"));
                self.log(chalk.greenBright.bold("[LOG]: Success! " + figures.tick));
                self.log(chalk.greenBright.bold("Size: " + Math.floor(archive.pointer() / 1000000) + " MB"));
                self.log(chalk.bold.greenBright("────────────────────────────────────────"));
                self.log('');
                cb()
            });

            archive.on('warning', function(err) {
                bar1.stop()
                clearInterval(myVar);
                self.log(chalk.bold.yellowBright(""));
                self.log(chalk.bold.yellowBright("────────────────────────────────────────"));
                self.log(chalk.bold.yellowBright("[LOG]: Warning! " + figures.cross + err));
                self.log(chalk.bold.yellowBright("────────────────────────────────────────"));
                cb()
            });

            archive.on('error', function(err) {
                bar1.stop()
                clearInterval(myVar);
                self.log(chalk.bold.redBright(""));
                self.log(chalk.bold.redBright("────────────────────────────────────────"));
                self.log(chalk.bold.redBright("[LOG]: Error! " + figures.cross + err));
                self.log(chalk.bold.redBright("────────────────────────────────────────"));
                cb()
            });
        });
    }).cancel(function () {
    output.close()
});
;


vorpal
    .command('backup group', 'Backup a group of folders')
    .action(function (args, cb) {
        const self = this;
        this.prompt({
            type: 'input',
            name: 'group',
            message: 'Which group should be archived? '
        }, function (result) {

                let group = result.group
                group = group.toLowerCase()

            if(db.locations.groups[group] != undefined){



            output = fs.createWriteStream(db.locations.groups[group].savePath + '/' + moment().format('DDMMYYYY-HHmmss').toString() + '_Backup.zip');
            let archive = archiver('zip', {
                zlib: {level: 9}
            });


                self.log('');
                self.log(chalk.bold.greenBright("────────────────────────────────────────"));
                self.log(chalk.greenBright.bold("Collecting Files..."))

            let paths = db.locations.groups[result.group].dirs;
            let groupFileSize = 0;
            let groupFileCount = 0;

                for (let i = 0; i < paths.length; i++) {

                archive.directory(paths[i], path.basename(paths[i]))
                self.log("+ " + path.basename(paths[i].toString()));
                let totalFileSize = fsUtils.fsizeSync(paths[i])

                let filecount = 0
                fsUtils.walkSync(paths[i], {
                }, function (err, path, stats, next, cache) {
                    if (!err && stats.isFile()) {
                        filecount++;
                    }
                    next();
                });

                self.log(chalk.greenBright.bold("Size: "+formatBytes(totalFileSize)))
                self.log(chalk.greenBright.bold("Files: "+ filecount))

                    groupFileSize += totalFileSize;
                    groupFileCount += filecount;
            }

                self.log('');
                self.log(chalk.greenBright.bold("Total Size: "+formatBytes(groupFileSize)))
                self.log(chalk.greenBright.bold("Total Files: "+ groupFileCount))
                self.log('');
                self.log(chalk.bold.greenBright("────────────────────────────────────────"));
                self.log('');


            archive.finalize()
            archive.pipe(output);

                const bar1 = new _cliProgress.Bar({
                    format: 'Backup Progress |' + '{bar}' + '| {percentage}% || {value}/{total} Chunks || ETA {eta_formatted} || Running {duration_formatted}',
                    barCompleteChar: '\u2588',
                    barIncompleteChar: '\u2591',
                    stream: process.stdout,
                    barsize: 32,
                }, _cliProgress.Presets.shades_classic);

                chalk.greenBright.bold(bar1.start(100, 0));

            let myVar = setInterval(function(){ timer() }, 1000);

            function timer() {
                    var percent = Math.floor(100-((groupFileSize - archive.pointer()) / groupFileSize) * 100);
                    bar1.update(percent)
                }




                output.on('close', function () {
                    bar1.stop()
                    clearInterval(myVar);
                    self.log('');
                    self.log(chalk.bold.greenBright("────────────────────────────────────────"));
                    self.log(chalk.greenBright.bold("[LOG]: Success! " + figures.tick));
                    self.log(chalk.greenBright.bold("Size: " + Math.floor(archive.pointer() / 1000000) + " MB"));
                    self.log(chalk.bold.greenBright("────────────────────────────────────────"));
                    self.log('');
                    cb()
                });

                archive.on('warning', function(err) {
                    bar1.stop()
                    clearInterval(myVar);
                    self.log(chalk.bold.yellowBright(""));
                    self.log(chalk.bold.yellowBright("────────────────────────────────────────"));
                    self.log(chalk.bold.yellowBright("[LOG]: Warning! " + figures.cross + err));
                    self.log(chalk.bold.yellowBright("────────────────────────────────────────"));
                    cb()
                });

                archive.on('error', function(err) {
                    bar1.stop()
                    clearInterval(myVar);
                    self.log(chalk.bold.redBright(""));
                    self.log(chalk.bold.redBright("────────────────────────────────────────"));
                    self.log(chalk.bold.redBright("[LOG]: Error! " + figures.cross + err));
                    self.log(chalk.bold.redBright("────────────────────────────────────────"));
                    cb()
                });


        }else{

                    self.log(chalk.bold.redBright(""));
                    self.log(chalk.bold.redBright("────────────────────────────────────────"));
                    self.log(chalk.bold.redBright("[LOG]: Error! " + figures.cross + " Gruppe nicht vorhanden!"));
                    self.log(chalk.bold.redBright("────────────────────────────────────────"));
                cb()
            }
        }
        );
    }).cancel(function () {
    output.close()
});
